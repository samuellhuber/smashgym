﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Foundation;
using KeyboardOverlap.Forms.Plugin.iOSUnified;
using UIKit;
using UserNotifications;

namespace SmashGym.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            KeyboardOverlapRenderer.Init(); //To prevent keyboard from overlapping Input

            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // Ask the user for permission to get notifications on iOS 10.0+
                UNUserNotificationCenter.Current.RequestAuthorization(
                    UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                    (approved, error) => { });

                // Watch for notifications while app is active
                UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();
            }
            else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                // Ask the user for permission to get notifications on iOS 8.0+
                var settings = UIUserNotificationSettings.GetSettingsForTypes(
                    UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                    new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }

            //enable backgrounding
            UIApplication.SharedApplication.BeginBackgroundTask(() => { });

            global::Xamarin.Forms.Forms.Init();
            OxyPlot.Xamarin.Forms.Platform.iOS.PlotViewRenderer.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
        //Open plan
        public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
        {
            //Subtract "file://" from beginning of the path
            string filePath = url.AbsoluteString.Substring(7);

            //Load XML file to be opened
            XmlAttributeOverrides attrOver = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            XmlElementAttribute attr = new XmlElementAttribute() { Type = typeof(Plan), ElementName = "Exercises" };
            attrs.XmlElements.Add(attr);
            attrOver.Add(typeof(Plan), "Exercises", attrs);
            //Create StreamReader to Load XML
            StreamReader fs = new StreamReader(filePath);
            //Create Deserializer
            XmlSerializer d = new XmlSerializer(typeof(Plan), attrOver);

            Plan _tempPlan = (Plan)d.Deserialize(fs);
            fs.Close();

            //Create Plan in SmashGym APP folder structure
            PlanSystem p = new PlanSystem();
            p.Create(_tempPlan);
            p.SaveCPlan(-2);

            //Launch XamarinForms App
            FinishedLaunching(app, options);

            //Delete file in iOS directory (keep the copy in SmashGym Folder)
            File.Delete(filePath);

            return true;
        }
    }
}
