﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Essentials;

namespace SmashGym
{
    public partial class GetPlans : ContentPage
    {
        public GetPlans()
        {
            InitializeComponent();
            Browser.Source = "https://smashice.de/free";
            Browser.Navigating += Handle_Navigating;
        }

        void BttnBackClicked(object sender, EventArgs e) 
        {
            Application.Current.MainPage = new MainPage(0);
        }

        void Handle_Navigating(object sender, Xamarin.Forms.WebNavigatingEventArgs e)
        {
            //Console.WriteLine("Navigation to: " + e.Url);
            if (e.Url.Contains("download"))
            {
                //Device.OpenUri(url);
                if (Device.RuntimePlatform != Device.Android)
                {
                    System.Uri url = new Uri(e.Url);
                    Device.OpenUri(url);
                }
            }
        }
    }
}
