﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;

namespace SmashGym
{
    public partial class ArchivUI : ContentPage
    {
        HashSet<string> fileNames = new HashSet<string>();

        public ArchivUI()
        {
            InitializeComponent();

            List<FileInfo> fileList = GetFiles();

            foreach(FileInfo f in fileList)
            {
                fileNames.Add(f.Name);
                Console.WriteLine(f.Name);
            }

            LVmain.ItemsSource = DisplayNames();
        }

        private List<string> DisplayNames() 
        {
            List<string> Display = fileNames.ToList();

            for(int i = 0; i < Display.Count; i++)
            {
                Display[i] = Display[i].Substring(0, Display[i].Length - 21);
            }

            Display = Display.Distinct().ToList();

            return Display;
        }

        void LVmain_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            List<string> fileNamesList = fileNames.ToList(); //to get all FileNames

            List<string> PlanList = DisplayNames(); //to get selected Plan Name

            string fileN = ""; //name of file to selected Plan

            //Match selected Plan with File
            foreach (string fileName in fileNamesList)
            {
                if (fileName.Substring(0, fileName.Length - 21) == PlanList[e.ItemIndex])
                {
                    fileN = fileName;
                    break;
                }
            }

            var path = Path.Combine("/log/full/" + fileN);
            PlanSystem p = new PlanSystem();
            Plan temp = p.Load(path);
            Navigation.PushModalAsync(new ArchivOverview(temp));
        }

        private List<FileInfo> GetFiles() 
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/log/full");
            DirectoryInfo dPath = new DirectoryInfo(path);

            //Create Log if it does not already exist
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<FileInfo> files = dPath.GetFiles().OrderBy(f => f.Name).ToList();

            return files;
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new MainPage(2);
        }
    }
}
