﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SmashGym
{
    public partial class FinishedOverview : ContentPage
    {
        Plan currentPlan;

        public FinishedOverview()
        {
            InitializeComponent();
        }
        public FinishedOverview(Plan _currentPlan)
        {
            currentPlan = _currentPlan;

            InitializeComponent();

            LblTitel.Text = currentPlan.day[currentPlan.cDay].dName;

            //----------------- SET UP GRID ------------------
            Grid g = new Grid();

            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });

            int counter = 0; //Count rows

            //Generate Overview
            for (int i = 0; i < currentPlan.day[currentPlan.cDay].Exercise.Length; i++)
            {
                //Create Exercise Name Label and add it to Grid
                Label lbl = new Label
                {
                    Text = currentPlan.day[currentPlan.cDay].Exercise[i].exName,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.Center
                };
                g.RowDefinitions.Add(new RowDefinition());
                g.RowDefinitions.Add(new RowDefinition());
                g.RowDefinitions.Add(new RowDefinition());
                g.Children.Add(lbl, 0, counter + 1);
                counter = counter +3;

                for (int j = 0; j < currentPlan.day[currentPlan.cDay].Exercise[i].sets; j++)
                {
                    int set = j + 1;

                    Label lblSet = new Label {
                        Text = "Set " + set,
                    };

                    g.Children.Add(lblSet, 0, counter);

                    Label lblReps = new Label
                    {
                        Text = currentPlan.day[currentPlan.cDay].Exercise[i].repsDone[j].ToString() + "x",
                        TextColor = Color.Black,
                    };

                    g.Children.Add(lblReps, 1, counter);

                    Label lblWeight = new Label {
                        Text = currentPlan.day[currentPlan.cDay].Exercise[i].weightDone[j] + "kg",
                        TextColor = Color.Black, 
                     };

                    g.Children.Add(lblWeight, 2, counter);
                    g.RowDefinitions.Add(new RowDefinition());
                    //g.RowDefinitions.Add(new RowDefinition());
                    counter = counter +1;
                }
            }

            //Add finished Grid g to Layout
            sLayout.Children.Add(g);
        }

        public void BttnContinueClicked(object sender, EventArgs e)
        {
            currentPlan.cData.cancelled = false;

            //Save current workout to LOG
            currentPlan.Log();

            //Save progress for trophies
            User user = new User();
            foreach (Exercises ex in currentPlan.day[currentPlan.cDay].Exercise)
            {
                for (int i = 0; i < ex.sets; i++)
                {
                    user.trophies.reps += Convert.ToInt16(ex.repsDone[i]);
                    user.trophies.weight += ex.weightDone[i];
                }
            }
            user.trophies.exercises += currentPlan.day[currentPlan.cDay].Exercise.Length;
            user.trophies.trainings++;
            user.Add();

            //End Workout UI
            currentPlan.cDay++;
            PlanSystem p = new PlanSystem();
            p.Create(currentPlan);

            p.SaveTempPlan(null);

            Application.Current.MainPage = new MainPage(0);
        }
    }
}