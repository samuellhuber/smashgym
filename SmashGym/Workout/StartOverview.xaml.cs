﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmashGym
{
    public partial class StartOverview : ContentPage
    {
        Plan currentPlan = null;

        public StartOverview()
        {
            InitializeComponent();
        }
        public StartOverview(Plan _currentPlan)
        {
            currentPlan = _currentPlan;

            InitializeComponent();

            LblTitel.Text = currentPlan.day[currentPlan.cDay].dName;

            //Generate Overview --!! 0 needs to be made dynamic !!--
            for (int i = 0; i < currentPlan.day[currentPlan.cDay].Exercise.Length; i++)
            {
                string reps = currentPlan.day[currentPlan.cDay].Exercise[i].reps.ToString();
                if (reps == "0") //When using maxreps option show "Max"
                    reps = "Max";

                Label lbl = new Label
                {
                    Text = currentPlan.day[currentPlan.cDay].Exercise[i].exName + " - " + currentPlan.day[currentPlan.cDay].Exercise[i].sets + " x " + reps,
                    TextColor = Color.Black,
                    HorizontalOptions = LayoutOptions.Center
                };

                //Check if exercise is part of superset to set text of Superset to Italic
                FontAttributes superset = FontAttributes.None;
                if (currentPlan.day[currentPlan.cDay].exList[i].superset == true)
                    superset = FontAttributes.Bold;
                else if (i != 0)
                {
                    if (currentPlan.day[currentPlan.cDay].exList[i - 1].superset == true)
                        superset = FontAttributes.Bold;
                }
                else
                    superset = FontAttributes.None;

                lbl.FontAttributes = superset;

                sLayout.Children.Add(lbl);
            }
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to Main HomePage in TabbedBar (MainPage Tabbed Focus Home) 
            Application.Current.MainPage = new MainPage(0);
        }

        public void BttnContinueClicked(object sender, EventArgs e)
        {
            //Start Workout with first exercise
            Application.Current.MainPage = new EnterWorkout(currentPlan);

            User user = new User();
            user.Load();
            user.SaveTemp();
        }
    }
}
