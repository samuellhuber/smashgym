﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;
using System.Threading.Tasks;
using Plugin.LocalNotifications;
using Plugin.Vibrate;

namespace SmashGym
{
    public partial class EnterWorkout : ContentPage
    {
        Plan currentPlan;
        PlanSystem p = new PlanSystem();
        uint cEx = 0; //CurrentExercise
        uint cSet = 0; //CurrentSet
        bool done = false; //State of Workout

        public EnterWorkout()
        {
            InitializeComponent();
        }

        //Coming from Cancelled Plan
        public EnterWorkout(Plan _currentPlan, uint _cEx, uint _cSet)
        {
            cEx = _cEx;
            cSet = _cSet;

            InitializeComponent();
            currentPlan = _currentPlan;

            MotivationQuotes m = new MotivationQuotes();
            LblMotivation.Text = m.Get();

            SetLabels();
        }

        //Normal Flow
        public EnterWorkout(Plan _currentPlan)
        {
            InitializeComponent();
            currentPlan = _currentPlan;

            MotivationQuotes m = new MotivationQuotes();
            LblMotivation.Text = m.Get();

            SetLabels();
        }

        //SetLabels to next Exercise/Set
        private void SetLabels()
        {
            //Set UI to next Exercise or Set
            EntryRepsDone.Text = "";
            EntryWeightDone.Text = "";

            LblDay.Text = "Tag " + currentPlan.day[currentPlan.cDay].dName;
            LblReps.Text = currentPlan.day[currentPlan.cDay].Exercise[cEx].repsDone[cSet].ToString();//Last workout - needs to be loaded from archive
            LblWeight.Text = currentPlan.day[currentPlan.cDay].Exercise[cEx].weightDone[cSet].ToString();//Last workout - needs to be loaded from archive
            LblcEx.Text = currentPlan.day[currentPlan.cDay].Exercise[cEx].exName + " - Satz " + ++cSet + " von " + currentPlan.day[currentPlan.cDay].Exercise[cEx].sets;
            LblPlannedReps.Text = currentPlan.day[currentPlan.cDay].Exercise[cEx].reps.ToString();
            if (currentPlan.day[currentPlan.cDay].Exercise[cEx].reps == 0) //when using max reps option show "Max" & Adjust UI
            {
                LblPlannedReps.Text = "Max";
                EntryWeightDone.Text = "0";
                EntryWeightDone.IsEnabled = false;
            }

            //Set Label weight to heighest weight from last session
            double maxWeight = 0;

            foreach (double d in currentPlan.day[currentPlan.cDay].Exercise[cEx].weightDone)
            {
                if (d > maxWeight)
                    maxWeight = d;
            }
            LblWeight.Text = maxWeight.ToString();

            //If too many reps were done indicate that weight may be too easy
            if (Convert.ToInt32(LblReps.Text) > currentPlan.day[currentPlan.cDay].Exercise[cEx].reps)
                LblReps.TextColor = Color.Red;
            else
                LblReps.TextColor = Color.Green;
        }

        public async void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to Main HomePage in TabbedBar (MainPage Tabbed Focus Home)
            if (currentPlan != null)
            {
                ////Finish remaining sets with 0 reps and 0 weight
                //for (uint i = cEx; i < currentPlan.day[currentPlan.cDay].Exercise.Length; i++)
                //{
                //    for (uint j = 0; j < currentPlan.day[currentPlan.cDay].Exercise[i].sets; j++)
                //    {
                //        //Check to not overwrite already done Sets of the current Exercise
                //        if (i == cEx && j == 0 && cSet < currentPlan.day[currentPlan.cDay].Exercise[i].sets)
                //        {
                //            j = cSet;
                //        }
                //        currentPlan.day[currentPlan.cDay].Exercise[i].weightDone[j] = 0;
                //        currentPlan.day[currentPlan.cDay].Exercise[i].repsDone[j] = 0;
                //    }
                //}

                currentPlan.cData.cancelled = true;
                currentPlan.cData.cEx = cEx;

                //Correct cSet
                uint _cSet = cSet;
                if (_cSet != 0)
                    _cSet -= 1;
                currentPlan.cData.cSet = _cSet;

                p.Create(currentPlan);
            }
            bool exit = await DisplayAlert("Achtung", "Möchtest du das Training wirklich abbrechen? im Log wird nichts gespeichert.", "Ja", "Nein");
            //Exit workout if confirmed by user
            if (exit == true)
                Application.Current.MainPage = new MainPage(0);
        }

        public void BttnSkipClicked(object sender, EventArgs e)
        {
            //Allow user to skip exercise and set weight and reps for remaining sets to 0
            for (uint j = 0; j < currentPlan.day[currentPlan.cDay].Exercise[cEx].sets; j++)
            {
                currentPlan.day[currentPlan.cDay].Exercise[cEx].weightDone[j] = 0;
                currentPlan.day[currentPlan.cDay].Exercise[cEx].repsDone[j] = 0;
            }

            cEx++;
            cSet = 0;

            if (cEx < currentPlan.day[currentPlan.cDay].Exercise.Length)
            {
                ////Set UI to next Exercise or Set
                SetLabels();
            }
            else
            {
                done = true;
                Application.Current.MainPage = new FinishedOverview(currentPlan);
            }
        }

        public void EntryRepsDoneTextChanged(object sender, EventArgs e)
        {
            //Numbers only
            if (EntryRepsDone.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryRepsDone.Text = ""; }
        }
        public void EntryWeightDoneTextChanged(object sender, EventArgs e)
        {
            //Floating points only
            if (float.TryParse(EntryWeightDone.Text, out float temp)) { }
            else if (EntryWeightDone.Text != ""){ DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryWeightDone.Text = ""; }
        }

        private Task<int> TimerAsync()
        {
            uint pause = currentPlan.day[currentPlan.cDay].Exercise[cEx].pause;

            StackLayout s = new StackLayout { VerticalOptions = LayoutOptions.Center };
            Content = s;

            Label lblTime = new Label
            {
                HorizontalOptions = LayoutOptions.Center,
                TextColor = Color.Black,
                Scale = 4,
                Margin = 25,
                Text = pause.ToString()
            };
            s.Children.Add(lblTime);

            string Name = ""; //SET Name of display "next Exercise" based on what exercise is really next
            string Reps = currentPlan.day[currentPlan.cDay].Exercise[cEx].reps.ToString(); //SET Reps of display based on what is really next
            if (Reps == "0")//If exercise.reps = 0 set Reps Label to "Max"
                Reps = "Max";

            //Check for superset
            if (cSet == currentPlan.day[currentPlan.cDay].Exercise[cEx].sets || currentPlan.day[currentPlan.cDay].Exercise[cEx].superset == true)
            {
                if (!done && currentPlan.day[currentPlan.cDay].exList.Count != cEx + 1)
                {
                    Name = currentPlan.day[currentPlan.cDay].Exercise[cEx + 1].exName;
                    //Set 1st label right for superset
                    if(currentPlan.day[currentPlan.cDay].Exercise[cEx + 1].superset == true && currentPlan.day[currentPlan.cDay].exList.Count > cEx + 2)
                        Name = currentPlan.day[currentPlan.cDay].Exercise[cEx + 1].exName + "\nund dann: " + currentPlan.day[currentPlan.cDay].Exercise[cEx+2].exName;

                    Reps = currentPlan.day[currentPlan.cDay].Exercise[cEx + 1].reps.ToString();
                    if (Reps == "0")//If exercise.reps = 0 set Reps Label to "Max"
                        Reps = "Max";
                }
            }
            else if (cEx != 0)
            {
                //check for superset
                if (currentPlan.day[currentPlan.cDay].Exercise[cEx - 1].superset == true)
                {
                    Name = currentPlan.day[currentPlan.cDay].Exercise[cEx - 1].exName + "\nund dann: " + currentPlan.day[currentPlan.cDay].Exercise[cEx].exName;
                    Reps = currentPlan.day[currentPlan.cDay].Exercise[cEx - 1].reps.ToString();
                    if (Reps == "0")//If exercise.reps = 0 set Reps Label to "Max"
                        Reps = "Max";
                }
                else
                {
                    Name = currentPlan.day[currentPlan.cDay].Exercise[cEx].exName;
                }
            }
            else //default none superset
            {
                Name = currentPlan.day[currentPlan.cDay].Exercise[cEx].exName;
            }

            Label lblNextEx = new Label
            {
                Margin = 10,
                Text = "Nächste Übung: " + Name
                + "\n Wiederholungen: " + Reps,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
            };
            s.Children.Add(lblNextEx);

            Button bttnSkipPause = new Button
            {
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.End,
                TextColor = Color.Black,
                BackgroundColor = Color.Aqua,
                Margin = 25,
                CornerRadius = 10,
                Text = "Pause überspringen"
            };
            bttnSkipPause.Clicked += (object sender, EventArgs e) =>
            {
                pause = 0;
            };

            s.Children.Add(bttnSkipPause);

            if (!done) //Check to not show notification when workout is done
            {
                //Countdown Timer
                Device.StartTimer(TimeSpan.FromSeconds(1), () =>
                {
                    lblTime.Text = pause.ToString();

                    // True = Repeat again, False = Stop the timer
                    if (pause == 0 && !done) //!done to not get break push notification when workout is done
                    {
                        Content = UI;
                        UI.IsVisible = true;
                        bool notification = true; //Wheter or not to show notification

                        if (cEx != 0)
                        {
                            if (currentPlan.day[currentPlan.cDay].Exercise[cEx-1].superset == true)
                            {
                                //Do not show alert when superset
                                notification = false;
                                DisplayAlert("Supersatz", "Supersatz = KEINE Pause;","Verstanden");
                            }
                        }

                        if(notification)
                        {
                            //Show notification and vibrate device
                            CrossLocalNotifications.Current.Show("PAUSE FERTIG", "MACH DICH RAN!");
                            var v = CrossVibrate.Current;
                            v.Vibration(TimeSpan.FromSeconds(5));
                        }

                        EntryRepsDone.Unfocus(); //make sure no Entry is instantly focused - let user tap entry manually
                        return false;
                    }
                    else
                    {
                        pause--;
                        return true;
                    }
                });
            }

            return Task.FromResult(0);
        }

        public async void BttnConfirmClicked(object sender, EventArgs e)
        {
            if (EntryRepsDone.Text != null && EntryWeightDone.Text != null)
            {
                EntryWeightDone.Text = EntryWeightDone.Text.Replace(",", "."); //Replace , with . to correctly get floating point entries

                bool confirm = true; //Let user confirm input is correct
                confirm = await DisplayAlert("Stimmt die Eingabe?","Gewicht: " + EntryWeightDone.Text + "\nWiederholungen " + EntryRepsDone.Text, "Ja", "Nein");

                if(confirm) //If user input is confirmed to be correct
                {

                    if (EntryRepsDone.Text.All(char.IsDigit) && float.TryParse(EntryWeightDone.Text, out float tempWeight))
                    {
                        //Timer
                        UI.IsVisible = false;
                        int temp = await TimerAsync();

                        //Save current Set
                        currentPlan.day[currentPlan.cDay].Exercise[cEx].repsDone[cSet - 1] = Convert.ToUInt16(EntryRepsDone.Text);
                        currentPlan.day[currentPlan.cDay].Exercise[cEx].weightDone[cSet - 1] = tempWeight;

                        //Next Set
                        cSet = cSet++;

                        //Backup current Plan to be able to resume when backgrounding kills app
                        currentPlan.cData.cancelled = true;
                        currentPlan.cData.cEx = cEx;
                        currentPlan.cData.cSet = cSet;

                        //Store values needed for resuming when backgrounding kills app
                        p.SaveTempPlan(currentPlan);

                        //If all sets done, next exercise
                        if (cSet == currentPlan.day[currentPlan.cDay].Exercise[cEx].sets)
                        {

                        if (cEx == currentPlan.day[currentPlan.cDay].Exercise.Length - 1)
                        {
                            //Workout Done
                            done = true;
                            Application.Current.MainPage = new FinishedOverview(currentPlan);
                        }
                        else
                        {
                            //Start next exercise if not superset, if superset - dont
                            if (currentPlan.day[currentPlan.cDay].Exercise[cEx].superset == true)
                            {
                                //empty
                            }
                            else
                            {
                                cSet = 0;
                                cEx++;
                            }
                        }
                    }

                    if (currentPlan.day[currentPlan.cDay].Exercise[cEx].superset == true && cSet != 0) //If 1st ex of superset
                    {
                        cSet = cSet - 1; //Keep same set
                        cEx++; //go to next exercise of superset
                    }
                    else if (cEx != 0)
                    {
                        uint tempcEx = cEx - 1; //temp for if statement, because cEx-- would decrease cEx even outside the ifCheck
                        if (currentPlan.day[currentPlan.cDay].Exercise[tempcEx].superset == true) //If 2nd ex of superset
                        {
                            //Keep cSet & go back to 1st ex
                            cEx = cEx - 1;
                        }
                    }
                    if (cSet != currentPlan.day[currentPlan.cDay].Exercise[cEx].sets)
                    {
                        SetLabels();
                    }
                }
                else { await DisplayAlert("Fehler", "Bitte geschaffte Wiederholungen und Gewicht eingeben!", "OK"); }
            }
            else //when input is not correct stay on same page and let user correct input
            { }
            }
            else { await DisplayAlert("Fehler", "Bitte geschaffte Wiederholungen und Gewicht eingeben!", "OK"); }
        }
    }
}
