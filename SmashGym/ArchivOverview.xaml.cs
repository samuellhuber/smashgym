﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmashGym
{
    public partial class ArchivOverview : ContentPage
    {
        Plan currentPlan;

        public ArchivOverview()
        {
            InitializeComponent();
        }

        public ArchivOverview(Plan _currentPlan)
        {
            currentPlan = _currentPlan;

            InitializeComponent();

            LblTitel.Text = currentPlan.pName;

            for (currentPlan.cDay = 0; currentPlan.cDay < currentPlan.day.Length; currentPlan.cDay++)
            {
                Label lblDay = new Label
                {
                    Margin = 10,
                    Text = currentPlan.day[currentPlan.cDay].dName,
                    FontAttributes = FontAttributes.Bold,
                    HorizontalOptions = LayoutOptions.CenterAndExpand,
                };
                sLayout.Children.Add(lblDay);

                //Generate Overview
                for (int i = 0; i < currentPlan.day[currentPlan.cDay].Exercise.Length; i++)
                {
                    Label lbl = new Label
                    {
                        Text = currentPlan.day[currentPlan.cDay].Exercise[i].exName + " - " + currentPlan.day[currentPlan.cDay].Exercise[i].sets + " x " + currentPlan.day[currentPlan.cDay].Exercise[i].reps,
                        TextColor = Color.Black,
                        HorizontalOptions = LayoutOptions.Center,
                    };
                    sLayout.Children.Add(lbl);
                }
            }
        }
        public void BttnBackClicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }
    }
}
