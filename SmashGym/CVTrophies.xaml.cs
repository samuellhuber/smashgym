﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace SmashGym
{
    public partial class CVTrophies : ContentView
    {
        User user = new User();

        //Define levels
        int bronze = 1;
        int silver = 10;
        int gold = 100;
        int diamond = 1000;

        public CVTrophies()
        {
            InitializeComponent();

            Task.Delay(2000); //wait 2s
            user.Load(); //Load User
            InitProgress();
        }

        //Set different trophies (bronze,silver,gold,diamond)
        private void InitProgress()
        {
            //Init trophies
            InitTrainings();
            InitReps();
            InitWeight();
            InitExercises();
        }

        public void InitTrainings()
        {
            if(user.trophies.trainings >= bronze && user.trophies.trainings < silver)//Check for bronze
            {
                RewardTrainigsDone.Source = "cowboy_bronze.png";
            }
            else if(user.trophies.trainings >= silver && user.trophies.trainings < gold)//Check for silver
            {
                RewardTrainigsDone.Source = "cowboy_silver.png";
            }
            else if(user.trophies.trainings >= gold && user.trophies.trainings < diamond)//Check for gold
            {
                RewardTrainigsDone.Source = "cowboy_gold.png";
            }
            else if(user.trophies.trainings >= diamond)//Check for diamond
            {
                RewardTrainigsDone.Source = "cowboy_diamond.png";
            }
            else //no trophy
            {
                RewardTrainigsDone.Source = "cowboy_std.png";
            }
        }

        public void InitReps()
        {
            if (user.trophies.reps >= bronze && user.trophies.reps < silver)//Check for bronze
            {
                RewardRepsDone.Source = "fire_bronze.png";
            }
            else if (user.trophies.reps >= silver && user.trophies.reps < gold)//Check for silver
            {
                RewardRepsDone.Source = "fire_silver.png";
            }
            else if (user.trophies.reps >= gold && user.trophies.reps < diamond)//Check for gold
            {
                RewardRepsDone.Source = "fire_gold.png";
            }
            else if (user.trophies.reps >= diamond)//Check for diamond
            {
                RewardRepsDone.Source = "fire_diamond.png";
            }
            else //no trophy
            {
                RewardRepsDone.Source = "fire_std.png";
            }
        }

        public void InitWeight()
        {
            if (user.trophies.weight >= bronze && user.trophies.weight < silver)//Check for bronze
            {
                RewardWeightDone.Source = "weight_bronze.png";
            }
            else if (user.trophies.weight >= silver && user.trophies.weight < gold)//Check for silver
            {
                RewardWeightDone.Source = "weight_silver.png";
            }
            else if (user.trophies.weight >= gold && user.trophies.weight < diamond)//Check for gold
            {
                RewardWeightDone.Source = "weight_gold.png";
            }
            else if (user.trophies.weight >= diamond)//Check for diamond
            {
                RewardWeightDone.Source = "weight_diamond.png";
            }
            else //no trophy
            {
                RewardWeightDone.Source = "weight_std.png";
            }
        }

        public void InitExercises()
        {
            if (user.trophies.exercises >= bronze && user.trophies.exercises < silver)//Check for bronze
            {
                RewardExercisesDone.Source = "bizeps_bronze.png";
            }
            else if (user.trophies.exercises >= silver && user.trophies.exercises < gold)//Check for silver
            {
                RewardExercisesDone.Source = "bizeps_silver.png";
            }
            else if (user.trophies.exercises >= gold && user.trophies.exercises < diamond)//Check for gold
            {
                RewardExercisesDone.Source = "bizeps_gold.png";
            }
            else if (user.trophies.exercises >= diamond)//Check for diamond
            {
                RewardExercisesDone.Source = "bizeps_diamond.png";
            }
            else //no trophy
            {
                RewardExercisesDone.Source = "biceps_std.png";
            }
        }

        public void BttnShopClicked(object sender, EventArgs e)
        {
            //Go to shop in default browser
            Application.Current.MainPage = new GetPlans();
            //Browser.OpenAsync("https://smashice.de/free");
        }
    }
}
