﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Microcharts;
using Xamarin.Forms;

namespace SmashGym
{
    public partial class Log : ContentPage
    {

        string[] log;

        public Log()
        {
            InitializeComponent();

            log = GetLog();

            GeneratePicker();

            if(PickerLog.Items.Count != 0)
                PickerLog.SelectedIndex = 0;
        }

        public string[] GetLog() 
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/log");
            DirectoryInfo dPath = new DirectoryInfo(path);

            //Create Log if it does not already exist
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            List<FileInfo> alphabeticFiles = dPath.GetFiles().OrderBy(f => f.Name).ToList();

            //Generate Array allEx of all Exercises found in Log
            HashSet<string> allExHash = new HashSet<string>();

            foreach (FileInfo o in alphabeticFiles)
            {
                bool xml = o.FullName.EndsWith(".xml");
                if (o.FullName.EndsWith(".xml") == true)
                {
                    int start = o.FullName.Length - 24;

                    string temp = o.FullName.Substring(o.FullName.LastIndexOf("log/"));
                    temp = temp.Remove(0, 4); //Cut log/ from string

                    allExHash.Add(temp);
                }
            }

            var allEx = allExHash.ToArray();
            int i = allExHash.Count;

            return allEx;
        }


        public void BttnArchivClicked(object sender, EventArgs e) 
        {
            Application.Current.MainPage = new ArchivUI();
        }

        public void GeneratePicker()
        {
            HashSet<string> exHesh = new HashSet<string>();

            PickerLog.SelectedIndex = 0;

            //Add all exercises to Hash
            foreach(string s in log)
            {
                string temp = s.Remove(s.IndexOf("_"));
                exHesh.Add(temp);
            }

            foreach (string s in exHesh)
            {
                PickerLog.Items.Add(s);
            }
        }

        public void OxyPlotGraph()
        {
            //Create Model to hold graph
            var model = new OxyPlot.PlotModel();

            double maximumValue = 150; //maximumValue for Y Axes

            //Create Graph
            var graphSeries = new OxyPlot.Series.LineSeries() { Color = OxyPlot.OxyColors.Aqua};
            
            //ADD DATA to graph
            string[] tempLog = GetLog();

            int counter = 0; //Count up position on x axes

            foreach (string s in tempLog)
            {
                if (s.Contains((string)PickerLog.SelectedItem))
                {
                    Exercises tempEx = Load(s);

                    for (int i = 0; i < tempEx.sets; i++)
                    {
                        //Set MaxValue for Chart based on highest weight
                        if (tempEx.weightDone[i] > maximumValue)
                            maximumValue = tempEx.weightDone[i];

                        string tempS = s.Substring(s.IndexOf("_") + 1);
                        tempS = tempS.Remove(tempS.LastIndexOf("_"));
                        DateTime date = DateTime.ParseExact(tempS, "ddMMyyyy", System.Globalization.CultureInfo.InvariantCulture);

                        double value = tempEx.weightDone[i];
                        //If value 0 (maybe due to max reps - weight being 0) then use reps
                        if (value == 0)
                            value = tempEx.repsDone[i];

                        //graphSeries.Points.Add(new OxyPlot.DataPoint(OxyPlot.Axes.DateTimeAxis.ToDouble(date),tempEx.weightDone[i]));
                        graphSeries.Points.Add(new OxyPlot.DataPoint(counter, value));
                        counter++;
                    }
                }
            }

            //Add Graph to Model
            model.Series.Add(graphSeries);

            //Add DateAxes to the bottom
            //model.Axes.Add(new OxyPlot.Axes.DateTimeAxis() { Position = OxyPlot.Axes.AxisPosition.Bottom, AbsoluteMaximum = OxyPlot.Axes.DateTimeAxis.ToDouble(DateTime.Now), StringFormat = "dd.MMM",});
            model.Axes.Add(new OxyPlot.Axes.LinearAxis { Position = OxyPlot.Axes.AxisPosition.Bottom, TextColor = OxyPlot.OxyColors.White, TicklineColor = OxyPlot.OxyColors.White, });
            //Add LinearAxes to the left
            model.Axes.Add(new OxyPlot.Axes.LinearAxis() { Position = OxyPlot.Axes.AxisPosition.Left, AbsoluteMinimum = 0, Minimum = 0, AbsoluteMaximum = maximumValue, LabelFormatter = AxesLabelFormatter,});

            //Add Model to View
            PlotView.Model = model;
        }

        private string AxesLabelFormatter(double d)
        {
            return String.Format("{0} kg", d);
        }

        public void PickerLog_Changed(object sender,EventArgs e)
        {
            OxyPlotGraph();
        }

        //Load Logged Exercises from an XML
        public Exercises Load(string _filename)
        {
            //Append given filename to Environment Path - ATTENTION: provided filename/_tempPath must include subfolders!
            string _tempPath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/log/" + _filename;

            //Prepare XML for being loaded
            XmlAttributeOverrides attrOver = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            XmlElementAttribute attr = new XmlElementAttribute() { Type = typeof(Set), ElementName = "Sets" };
            attrs.XmlElements.Add(attr);
            attrOver.Add(typeof(Exercises), "Exercises", attrs);
            //Create FileStream to Load XML
            FileStream fs = new FileStream(_tempPath, FileMode.Open);
            //Create Deserializer
            XmlSerializer d = new XmlSerializer(typeof(Exercises), attrOver);

            Exercises _temp = (Exercises)d.Deserialize(fs);
            fs.Close();

            return _temp;
        }
        /*
        public void GenerateMicroChart()
        {
            //Generate Graph
            List<ChartEntry> data = new List<ChartEntry>(); //Entries to Chart

            string[] tempLog = log.OrderBy(x => x).ToArray();
            float chartMin = 1000000;
            int exCount = 0;
            int cExCount = 0;
            HashSet<string> xAchsis = new HashSet<string>(); //make sure xachsis is only labeled once

            foreach (string str in tempLog)
            {
                if (str.Contains((string)PickerLog.SelectedItem))
                {
                    exCount++;
                }
            }

            foreach (string s in tempLog)
            {
                if (s.Contains((string)PickerLog.SelectedItem))
                {
                    Exercises tempEx = Load(s);
                    cExCount++;

                    //Set MinValue for Chart based on lowest weight
                    if (tempEx.weightDone[0] < chartMin)
                        chartMin = tempEx.weightDone[0];

                    for (int i = 0; i < tempEx.sets; i++)
                    {
                        ChartEntry tempEntry = new ChartEntry(tempEx.weightDone[i])
                        {
                            Label = Convert.ToString(tempEx.weightDone[i]) + " kg",
                            Color = SkiaSharp.SKColor.Parse("#00ffec"),
                            TextColor = SkiaSharp.SKColor.Parse("#000000"),
                        };

                        if (i == 0 && exCount % cExCount == 0)
                        {
                            System.Globalization.DateTimeFormatInfo date = new System.Globalization.DateTimeFormatInfo();

                            string tempS = s.Substring(s.IndexOf("_") + 3);
                            tempS = tempS.Remove(tempS.LastIndexOf("_") - 4);

                            string tempS2 = s.Substring(s.IndexOf("_") + 1);
                            tempS2 = tempS2.Remove(2);

                            tempEntry.ValueLabel = tempS2 + " " + date.GetMonthName(Convert.ToInt32(tempS));
                            tempEntry.Color = SkiaSharp.SKColor.Parse("#00ffec");
                            tempEntry.TextColor = SkiaSharp.SKColor.Parse("#f141f4");
                        }
                        else { tempEntry.ValueLabel = " "; }

                        //Only set xAchsis label if its unique
                        if (xAchsis.Contains(tempEntry.Label))
                        {
                            tempEntry.Label = "";
                        }
                        else
                            xAchsis.Add(tempEntry.Label);

                        //Add to data of Graph
                        data.Add(tempEntry);
                    }
                }
            }

            chartMin = chartMin - (chartMin / 10);

            CV_Main.Chart = new LineChart
            {
                Entries = data,
                LabelOrientation = Orientation.Vertical,
                ValueLabelOrientation = Orientation.Vertical,
                BackgroundColor = SkiaSharp.SKColor.Parse("#C0C0C0"),
                MinValue = chartMin,
                LabelTextSize = 40,
                LineMode = LineMode.Straight,
                PointMode = PointMode.None,
            };
        }*/
    }
}
