﻿using System;
namespace SmashGym
{
    public class Set
    {
        //Reps done in that particular set
        public short repsDone;
        //Weight done in that particular set
        public double weightDone;

        public Set(short _repsDone, double _weightDone)
        {
            repsDone = _repsDone;
            weightDone = _weightDone;

        }
    }
}
