﻿using System;
using System.Xml.Serialization;
using System.IO;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace SmashGym
{
    [XmlRoot(Namespace = "SmashGym", ElementName = "User", DataType = "string", IsNullable = true)]
    public class User
    {
        public string username { get; set; }
        public Int16 age { get; set; }
        public double weight { get; set; }
        //public double bodyfatpercentage { get; set; }
        public double userID { get; set; }
        //public int cPlan { get; set; } //Default -1 for Empty Picker
        //TO SAVE TROPHY PROGRESS
        public struct Trophies {
            public int trainings;
            public int reps;
            public float weight;
            public int exercises;
        }
        public Trophies trophies;

        public User()
        {
            
        }

        public void Add()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tempUserData.xml");

            var temp = DeserializeXMLFileToObject<User>(path);

            this.trophies.trainings += temp.trophies.trainings;
            this.trophies.reps += temp.trophies.reps;
            this.trophies.weight += temp.trophies.weight;
            this.trophies.exercises += temp.trophies.exercises;

            Save();

        }

        public void Load()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "UserData.xml");

            var _tempUser = DeserializeXMLFileToObject<User>(path);

            if (_tempUser != null)
            {
                //Assign User Data to this
                this.username = _tempUser.username;
                this.age = _tempUser.age;
                this.weight = _tempUser.weight;
                //this.bodyfatpercentage = _tempUser.bodyfatpercentage;
                this.userID = _tempUser.userID;
                this.trophies.trainings = _tempUser.trophies.trainings;
                this.trophies.reps = _tempUser.trophies.reps;
                this.trophies.weight = _tempUser.trophies.weight;
                this.trophies.exercises = _tempUser.trophies.exercises;
                //this.cPlan = _tempUser.cPlan;
            }
        }

        public static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                using (StreamReader xmlStream = new StreamReader(XmlFilename))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    returnObject = (T)serializer.Deserialize(xmlStream);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString(), DateTime.Now);
            }
            return returnObject;
        }

        public void SaveTemp()
        {
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tempUserData.xml");
            //Create FileStream to Load XML
            using (StreamWriter fs = new StreamWriter(path))
            {
                //Serialize XML
                XmlRootAttribute xRoot = new XmlRootAttribute
                {
                    ElementName = "User",
                    IsNullable = true,
                    Namespace = "SmashGym",
                };
                XmlSerializer s = new XmlSerializer(typeof(User), xRoot);
                s.Serialize(fs, this);
                fs.Close();
            }
        }

        public void Save()
        {   
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "UserData.xml");
            //Create FileStream to Load XML
            using (StreamWriter fs = new StreamWriter(path))
            {
                //Serialize XML
                XmlRootAttribute xRoot = new XmlRootAttribute
                {
                    ElementName = "User",
                    IsNullable = true,
                    Namespace = "SmashGym",
                };
                XmlSerializer s = new XmlSerializer(typeof(User), xRoot);
                s.Serialize(fs, this);
                fs.Close();
            }
        }
    }
}
