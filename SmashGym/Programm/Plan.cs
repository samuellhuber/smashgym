﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SmashGym
{
    public class Plan
    {
        public Days[] day;
        public Days lastDay;
        public uint cDay = 0;
        public string pName = "NONE";
        public struct CancelledData
        {
            public bool cancelled;
            public uint cEx;
            public uint cSet;
        }
        //When creating a new Plan set default values when initializing cData
        public CancelledData cData = new CancelledData() { cancelled = false, cEx = 0, cSet = 0,};

        public Plan() { }
        public Plan(string _name, UInt16 _days)
        {
            this.pName = _name; 
            this.day = new Days[_days];
            for (int i = 0; i < _days; i++)
            {
                day[i] = new Days();
            }
        }
        public Plan(UInt16 _days)
        {
            this.day = new Days[_days];
            for (int i = 0; i < _days; i++)
            {
                day[i] = new Days();
            }
        }

        public void Log()
        {
            //Log exercises for Graph in Archive
            for (int i = 0; i < day[cDay].Exercise.Length; i++)
            {
                //Set Name for XML Log File for each exercise to log performance per exercise
                string xmlName = day[cDay].Exercise[i].exName + "_" + String.Format("{0:ddMMyyyy}",DateTime.Today)+ "_" + DateTime.Now.Hour + DateTime.Now.Minute + "SmashLOG";

                //Prepare XML for Serialization
                XmlAttributes attrs = new XmlAttributes();
                XmlElementAttribute attr = new XmlElementAttribute() { Type = typeof(Set), ElementName = "Sets" };
                attrs.XmlElements.Add(attr);
                XmlAttributeOverrides attrOver = new XmlAttributeOverrides();
                attrOver.Add(typeof(Exercises), "Exercise", attrs);
                XmlSerializer s = new XmlSerializer(typeof(Exercises), attrOver);

                //Set Path of XML File
                var startPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/log/");
                string path = Path.Combine(startPath + xmlName + ".xml");
                //Create Stream to write file to path
                TextWriter writer = new StreamWriter(path);

                //Serialize Plan
                s.Serialize(writer, day[cDay].Exercise[i]);

                writer.Close();
            }

            //Log full plan for detailed Archive

            //Prepare XML for Serialization
            XmlAttributes attrs1 = new XmlAttributes();
            XmlElementAttribute attr1 = new XmlElementAttribute() { Type = typeof(Exercises), ElementName = "Exercises" };
            attrs1.XmlElements.Add(attr1);
            XmlAttributeOverrides attrOver1 = new XmlAttributeOverrides();
            attrOver1.Add(typeof(Plan), "Exercises", attrs1);
            XmlSerializer s1 = new XmlSerializer(typeof(Plan), attrOver1);

            //Set Path of XML File
            var startPath1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/log/full");
            string path1 = Path.Combine(startPath1, this.pName + String.Format("{0:ddMMyyyy}", DateTime.Today) + "_SmashLOG" + ".xml");
            //Create Stream to write file to path
            if(!Directory.Exists(startPath1))
            {
                Directory.CreateDirectory(startPath1);
            }
            TextWriter writer1 = new StreamWriter(path1);

            //Serialize Plan
            s1.Serialize(writer1, this);

            writer1.Close();
        }
    }
}
