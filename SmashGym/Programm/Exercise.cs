﻿using System;
namespace SmashGym
{
    public class Exercises
    {
        public string exName; 
        public uint sets = 1; 
        public uint reps = 1;
        public uint pause = 3;
        public bool superset = false;
        public float[] weightDone; 
        public uint[] repsDone;

        public Exercises()
        {
            this.weightDone = new float[sets];
            this.repsDone = new uint[sets];
        }

        public Exercises(string _name, uint _sets, uint _reps, uint _pause)
        {
            this.exName = _name;
            this.sets = _sets;
            this.reps = _reps;
            this.pause = _pause;
            //If pause == 0 set superset true
            superset |= pause == 0;

            this.weightDone = new float[sets];
            this.repsDone = new uint[sets];
        }
    }
}
