﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SmashGym
{
    public class PlanSystem
    {
        public PlanSystem()
        {
        }

        public void SaveCPlan(int cPlan)
        {
            TextWriter text = new StreamWriter(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),"cPlan.txt"));
            text.Write(cPlan);
            text.Close();
        }
        public int LoadCPlan()
        {
            if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "cPlan.txt")))
            {
                TextReader text = new StreamReader(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "cPlan.txt"));
                string cPlan = text.ReadLine();
                text.Close();
                return Convert.ToInt32(cPlan);
            }
            else
                return 0;
        }

        public void SaveTempPlan(Plan _tempPlan)
        {
            //Prepare XML for Serialization
            XmlAttributes attrs = new XmlAttributes();
            XmlElementAttribute attr = new XmlElementAttribute() { Type = typeof(Exercises), ElementName = "Exercises" };
            attrs.XmlElements.Add(attr);
            XmlAttributeOverrides attrOver = new XmlAttributeOverrides();
            attrOver.Add(typeof(Plan), "Exercises", attrs);
            XmlSerializer s = new XmlSerializer(typeof(Plan), attrOver);

            //Set Path of XML File
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tempplan" + ".xml");
            //Create Stream to write file to path
            TextWriter writer = new StreamWriter(path);

            //Serialize Plan
            s.Serialize(writer, _tempPlan);

            writer.Close();
        }

        //Create an XML Document for given Plan
        public void Create(Plan _tempPlan)
        {
            //Prepare XML for Serialization
            XmlAttributes attrs = new XmlAttributes();
            XmlElementAttribute attr = new XmlElementAttribute() { Type = typeof(Exercises), ElementName = "Exercises" };
            attrs.XmlElements.Add(attr);
            XmlAttributeOverrides attrOver = new XmlAttributeOverrides();
            attrOver.Add(typeof(Plan), "Exercises", attrs);
            XmlSerializer s = new XmlSerializer(typeof(Plan), attrOver);
            
            //Set Path of XML File
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), _tempPlan.pName + ".xml");
            //Create Stream to write file to path
            TextWriter writer = new StreamWriter(path);

            //Serialize Plan
            s.Serialize(writer, _tempPlan);

            writer.Close();
        }

        //Delete a certain XML
        public void Delete(string _filename)
        {
            string _tempPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), _filename);
            File.Delete(_tempPath);
        }

        //Load Plan from an XML
        public Plan Load(string _filename)
        {
            //Append given filename to Environment Path - ATTENTION: provided filename/_tempPath must include subfolders!
            string _tempPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/" +  _filename);

            //Prepare XML for being loaded
            XmlAttributeOverrides attrOver = new XmlAttributeOverrides();
            XmlAttributes attrs = new XmlAttributes();
            XmlElementAttribute attr = new XmlElementAttribute() { Type = typeof(Plan), ElementName = "Exercises" };
            attrs.XmlElements.Add(attr);
            attrOver.Add(typeof(Plan), "Exercises", attrs);
            //Create FileStream to Load XML
            FileStream fs = new FileStream(_tempPath, FileMode.Open);
            //Create Deserializer
            XmlSerializer d = new XmlSerializer(typeof(Plan), attrOver);

            Plan _tempPlan = (Plan)d.Deserialize(fs);
            fs.Close();

            return _tempPlan;
        }
    }
}
