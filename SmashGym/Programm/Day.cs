﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace SmashGym
{
    public class Days
    {
        public Exercises[] Exercise;
        public List<Exercises> exList = new List<Exercises>(); //List for new CreatePlan Flow
        public string dName;
        public Days() { }
        public Days(uint _nexercises)
        {
            this.Exercise = new Exercises[_nexercises];
            for (int i = 0; i < _nexercises; i++)
            {
                Exercise[i] = new Exercises();
            }
        }
    }
}