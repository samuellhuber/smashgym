﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SmashGym
{
    public partial class MainPage : TabbedPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        public MainPage(int i)
        {
            InitializeComponent();
            if (i < 3) 
            {
                this.CurrentPage = Children[i];
            }
        }
    }
}
