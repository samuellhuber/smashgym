﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmashGym
{

    public partial class RemoveExerciseUI : ContentPage
    {
        Plan tempPlan; //tempPlan from which to remove exercise

        public RemoveExerciseUI()
        {
            InitializeComponent();
        }

        public RemoveExerciseUI(Plan _tempPlan)
        {
            InitializeComponent();

            tempPlan = _tempPlan;

            CreateExList();
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Send adjusted Plan via MessagingCenter (CreatePlanUI is listening and getting new Plan)
            MessagingCenter.Send(this, "RemovedExPlan", tempPlan);

            Navigation.PopModalAsync();
        }

        void CreateExList()
        {
            //Grid holding Table/Data
            Grid g = new Grid { };
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(3, GridUnitType.Star) });
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(2, GridUnitType.Star) });

            int counter = -1; //count rows

            for (int i = 0; i < tempPlan.day[tempPlan.cDay].exList.Count; i++)
            {
                g.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                counter++;

                //Add Name to Grid
                Label lbl = new Label
                {
                    Text = tempPlan.day[tempPlan.cDay].exList[i].exName,
                    TextColor = Color.Black,
                };

                g.Children.Add(lbl, 0, counter);

                //Add Set to Grid
                Button bttnDel = new Button
                {
                    TabIndex = i, //Set tabindex to index of exercise
                    Text = "X",
                    TextColor = Color.Red,
                    Opacity = 0.5,
                    CornerRadius = 10,
                    HorizontalOptions = LayoutOptions.Center,
                    BackgroundColor = Xamarin.Forms.Color.FromHex("DCDCDC"),

                };
                bttnDel.Clicked += delegate {
                    //remove exercise and reload table
                    tempPlan.day[tempPlan.cDay].exList.RemoveAt(bttnDel.TabIndex);
                    CreateExList();
                };

                g.Children.Add(bttnDel, 1, counter);
            }

            //Add Grid g to Scrollview to display data
            Overview.Content = g;
        }
    }
}
