﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace SmashGym
{
    public partial class CreatePlanUI : ContentPage
    {
        private int cFlow = 0; //Controls Flow in Button BttnNext
        private uint cDay = 0; //Current Day in Plan creation
        private Plan tempPlan;

        public CreatePlanUI()
        {
            cFlow = 0; //Start flow from the beginning
            InitializeComponent();

            //Using MessagingCenter to get Plan from AddedExercisesUI into CreatePlanUI (subscribing to a specific message send by AddExercisesUI before being popped)
            MessagingCenter.Subscribe(this, "AddedExPlan", (AddExercisesUI sender, Plan plan) => 
                {
                    tempPlan = plan;

                    DayOverviewUI();
                }
            );

            MessagingCenter.Subscribe(this, "RemovedExPlan", (RemoveExerciseUI sender, Plan plan) =>
            {
                tempPlan = plan;

                DayOverviewUI();
            }
            );
        }

        //Flow when editing already created Plans
        public CreatePlanUI(Plan _plan)
        {
            InitializeComponent();
            EditPlan(_plan);
            
            //Using MessagingCenter to get Plan from AddedExercisesUI into CreatePlanUI (subscribing to a specific message send by AddExercisesUI before being popped)
            MessagingCenter.Subscribe(this, "AddedExPlan", (AddExercisesUI sender, Plan plan) =>
            {
                tempPlan = plan;

                DayOverviewUI();
            }
            );

            MessagingCenter.Subscribe(this, "RemovedExPlan", (RemoveExerciseUI sender, Plan plan) =>
            {
                tempPlan = plan;

                DayOverviewUI();
            }
            );
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to Main GymPage in TabbedBar (MainPage Tabbed Focus Home) 
            Application.Current.MainPage = new MainPage(0);
        }

        void BttnNext_Clicked(object sender, System.EventArgs e)
        {
            //switch to control Flow steps in Plan creation (get Days, get Name, get Exercises for each day, finish)
            switch (cFlow)
            {
                case 0:
                    if (!string.IsNullOrEmpty(EntryDays.Text))
                    {
                        if (Convert.ToInt16(EntryDays.Text) <= 7)
                        {
                            //Create new plan with set amount of days
                            tempPlan = new Plan(Convert.ToUInt16(EntryDays.Text))
                            {
                                cDay = cDay
                            };

                            //Set Name of the Day of exercise to Number of day so that it cant be empty
                            for(int i = 0; i < tempPlan.day.Length; i++)
                            {
                                int temp = i + 1;
                                tempPlan.day[i].dName = Convert.ToString(temp);
                            }

                            //Next step
                            LblText.Text = "Wie soll der Trainingsplan heißen?";
                            EntryDays.IsVisible = false;
                            EntryDays.IsEnabled = false;
                            EntryName.IsVisible = true;
                            cFlow++;
                        }
                        else { DisplayAlert("Fehler", "Du kannst nur an maximal 7 Tagen pro Woche trainieren!", "OK"); }
                    }
                    else { DisplayAlert("Fehler", "Bitte alle Felder ausfüllen!", "OK"); }
                    break;
                case 1:
                    if (!string.IsNullOrEmpty(EntryName.Text))
                    {
                        if (Regex.IsMatch(EntryName.Text, @"^[a-zA-Z0-9]+$")) //Check if Name of Plan is only Letters or Number for correct filenames
                        {
                            //Set Name of Plan
                            tempPlan.pName = EntryName.Text;

                            //Next
                            LblTextDay.Text = "Füge Tag " + (cDay + 1) + " Übungen hinzu!";
                            LblText.IsVisible = false;
                            EntryName.IsVisible = false;
                            EntryName.IsEnabled = false;
                            BttnNext.IsVisible = false;
                            BttnNext.IsEnabled = false;
                            BttnNextDay.IsVisible = true;
                            BttnAdd.IsVisible = true;
                            BttnDelete.IsVisible = true;

                            //Dont say continue on BttnNextDay when its the last day.
                            if (cDay == tempPlan.day.Count() - 1)
                                BttnNextDay.Text = "Speichern!";

                            LblTextDay.IsVisible = true;
                            /*LblOverviewName.IsVisible = true;
                            LblOverviewSets.IsVisible = true;
                            LblOverviewReps.IsVisible = true;*/

                            DayOverviewUI();
                            cFlow++;
                        }
                        else { DisplayAlert("Fehler", "Bitte nur Buchstaben (A-Z) oder Zahlen verwenden.", "OK"); }
                    }
                    else { DisplayAlert("Fehler", "Bitte alle Felder ausfüllen!", "OK"); }
                    break;
            }
        }

        void EditPlan(Plan _plan)
        {
            this.tempPlan = _plan;
            tempPlan.cDay = 0; //start editing with first day

            //Dont say continue on BttnNextDay when its the last day.
            if (cDay == tempPlan.day.Count() - 1)
                BttnNextDay.Text = "Speichern!";

            //Do not show initial Plan SetUp UI
            LblTextDay.Text = "Füge Tag " + (cDay + 1) + " Übungen hinzu!";
            LblTextDay.IsVisible = true;
            LblText.IsVisible = false;
            EntryName.IsVisible = false;
            EntryName.IsEnabled = false;
            EntryDays.IsVisible = false;
            EntryDays.IsEnabled = false;
            BttnNext.IsVisible = false;
            BttnNext.IsEnabled = false;
            BttnNextDay.IsVisible = true;
            BttnAdd.IsVisible = true;
            BttnDelete.IsVisible = true;

            //load UI
            DayOverviewUI();
        }

        void BttnNextDay_Clicked(object sender, EventArgs e)
        {
            if (tempPlan.day[tempPlan.cDay].exList.Count != 0)
            {
                //Make sure that last exercise doesnt have superset = true and set it to false if true
                var temp = tempPlan.day[cDay].exList.ToArray();
                int i = temp.Length - 1;
                if (temp[i].superset == true)
                    temp[i].superset = false;

                cDay++;
                tempPlan.cDay = cDay;
                if (cDay < tempPlan.day.Count()) //if not all days have been done yet
                {
                    //Adjust UI to new day
                    LblTextDay.Text = "Füge Tag: " + (cDay + 1) + " Übungen hinzu!";
                    DayOverviewUI();

                    //Dont say continue on BttnNextDay when its the last day.
                    if (cDay == tempPlan.day.Count() - 1)
                        BttnNextDay.Text = "Speichern!";
                }
                else
                {
                    //Set Array of Exercises which is used in Workout UI
                    foreach (Days d in tempPlan.day)
                    {
                        d.Exercise = d.exList.ToArray();
                    }

                    //Save created Plan as .xml
                    PlanSystem p = new PlanSystem();
                    p.Create(tempPlan);

                    //Go to Home
                    Application.Current.MainPage = new MainPage(0);
                }
            }
            else 
            {
                DisplayAlert("Fehler","Bitte füge deinem Trainingstag Übungen hinzu.","Ok");
            }
        }

        //Create Table with overview of all exercises in current Day
        void DayOverviewUI()
        {
            //Activate ScrollView for Overview Data Table
            Overview.IsVisible = true;

            //Grid holding Table/Data
            Grid g = new Grid { };
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(5, GridUnitType.Star) });
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) });
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1.5, GridUnitType.Star) });
            g.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            int counter = -1; //count rows

            g.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
            counter++;

            for (int i = 0; i < 4; i++)
            {
                string text = "";

                switch (i)
                {
                    case 0:
                        text = "Name";
                        break;
                    case 1:
                        text = "Sätze";
                        break;
                    case 2:
                        text = "Reps";
                        break;
                    case 3:
                        break;
                }

                Label lbl = new Label
                {
                    Text = text,
                    TextColor = Color.Black,
                };
                g.Children.Add(lbl, i, counter);
            }

            for(int i = 0; i < tempPlan.day[cDay].exList.Count; i++)
            {
                g.RowDefinitions.Add(new RowDefinition { Height = GridLength.Auto });
                counter++;

                //Check if exercise is part of superset to set text of Superset to Italic
                FontAttributes superset = FontAttributes.None;
                if (tempPlan.day[cDay].exList[i].superset == true)
                    superset = FontAttributes.Bold;
                else if (i != 0)
                {
                    if (tempPlan.day[cDay].exList[i - 1].superset == true)
                        superset = FontAttributes.Bold;
                }
                else
                    superset = FontAttributes.None;

                //Add Name to Grid
                Label lbl = new Label {
                    Text = tempPlan.day[cDay].exList[i].exName,
                    TextColor = Color.Black,
                    FontAttributes = superset,
                };

                //Add Set to Grid
                Label lbl1 = new Label
                {
                    Text = tempPlan.day[cDay].exList[i].sets.ToString(),
                    TextColor = Color.Black,
                };

                //Add Reps to Grid
                Label lbl2 = new Label
                {
                    Text = tempPlan.day[cDay].exList[i].reps.ToString(),
                    TextColor = Color.Black,
                };

                if (lbl2.Text == "0")//If exercise.reps = 0 set Reps Label to "Max"
                    lbl2.Text = "Max";

                g.Children.Add(lbl, 0, counter);
                g.Children.Add(lbl1, 1, counter);
                g.Children.Add(lbl2, 2, counter);
            }

            //Add Grid g to Scrollview to display data
            Overview.Content = g;
        }

        //Go to UI for adding exercises
        async void BttnAdd_Clicked(object sender, System.EventArgs e)
        {
            await Navigation.PushModalAsync(new AddExercisesUI(tempPlan));
        }

        //Go to UI for removing exercises
        async void BttnDelete_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new RemoveExerciseUI(tempPlan));
        }

        //Check that only numbers are entered
        void EntryDays_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (EntryDays.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryDays.Text = ""; }
        }
    }
}
