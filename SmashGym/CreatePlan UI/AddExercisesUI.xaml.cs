﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace SmashGym
{
    public partial class AddExercisesUI : ContentPage
    {
        private Plan tempPlan; //temporary Plan being created

        public AddExercisesUI()
        {
            InitializeComponent();
        }

        public AddExercisesUI(Plan _tempPlan)
        {
            InitializeComponent();

            tempPlan = _tempPlan;
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to CreatePlanUI
            Navigation.PopModalAsync();
        }

        void BttnAdd_Clicked(object sender, EventArgs e)
        {
            if (EntryExName.Text != "" && EntrySets.Text != "" && EntrySets.Text != "0" && EntryReps.Text != "" && EntryPause.Text != "")
            {
                Exercises ex = new Exercises(EntryExName.Text, Convert.ToUInt16(EntrySets.Text), Convert.ToUInt16(EntryReps.Text), Convert.ToUInt16(EntryPause.Text));

                var temp = tempPlan.day[tempPlan.cDay].exList.ToArray();
                int i = temp.Length - 1;
                
                //If exercise before the one being added right now has superset = true, set current ex sets to the same amount
                if (tempPlan.day[tempPlan.cDay].exList.Count != 0)
                {
                    if (temp[i].superset == true && ex.pause == 0)
                    {
                        DisplayAlert("Fehler", "Bitte wähle eine Pausenzeit größer 0.", "Ok");
                        return;
                    }

                    if (temp[i].superset == true)
                    {
                        ex.sets = temp[i].sets;
                    }
                }

                tempPlan.day[tempPlan.cDay].exList.Add(ex);

                MessagingCenter.Send(this, "AddedExPlan", tempPlan); //send tempPlan with added exercise to CreatePlanUI using MessagingCenter

                Navigation.PopModalAsync();
            }
            else
            {
                DisplayAlert("Fehler", "Bitte alle Felder ausfüllen", "OK");
            }
        }

        //Allow only numbers to be entered
        void EntrySets_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (EntrySets.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntrySets.Text = ""; }
        }
        //Allow only numbers to be entered
        void EntryReps_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (EntryReps.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryReps.Text = ""; }
        }
        //Allow only numbers to be entered
        void EntryPause_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
        {
            if (EntryPause.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryPause.Text = ""; }
        }

        void Handle_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (SwitchMaxReps.IsToggled == false)
            {
                EntryReps.IsEnabled = true;
                EntryReps.Text = "";
                EntryReps.TextColor = Color.Black;
            }
            else
            {
                EntryReps.Text = "0";
                EntryReps.IsEnabled = false;
            }
        }
    }
}
