﻿using System;
using System.Linq;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmashGym
{
    public partial class Calc1RM : ContentPage
    {
        public Calc1RM()
        {
            InitializeComponent();

            LblText.Text = "Bestimmung des 1RM Max \n\n" + 
                            "- Aufwärmen \n" +
                            "- Gewicht nehmen, mit dem du geschätzt 5-10 Wiederholungen schaffst \n" +
                            "- Gewicht und geschaffte Wiederholungen eintragen \n" +
                            "- Berechnen drücken!" ;
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go to MainPage with focus on Home
            Application.Current.MainPage = new MainPage(0);
        }

        public void EntryWeightChanged(object sender, EventArgs e)
        {
            //Numbers only
            if (EntryWeight.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryWeight.Text = ""; }
        }

        public void EntryRepsChanged(object sender, EventArgs e)
        {
            //Numbers only
            if (EntryReps.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryReps.Text = ""; }
        }

        public void BttnCalculateClicked(object sender, EventArgs e)
        { 
            if(EntryReps.Text != "" && EntryWeight.Text != "")
            {
                int reps = Convert.ToInt32(EntryReps.Text);
                int weight = Convert.ToInt32(EntryWeight.Text);

                float tempRes = Get1RM( reps , weight );
                if (tempRes == -1)
                {
                    EntryReps.Text = "";
                }
                else
                {
                    BttnCalculate.Text = tempRes.ToString("n2");
                }
            }
        }

        public float Get1RM(int _reps, int _weight)
        {
            float oneRM = 0;

            if(_reps <= 0 || _weight <= 0)
            { 
                DisplayAlert("Fehler","Wiederholungen & Gewicht müssen mehr als 0 sein!","OK");
                    return -1;
            }

            //Epley Formel
            oneRM = (float)_reps / 30;
            oneRM = oneRM * _weight + _weight;

            return oneRM;
        }
    }
}
