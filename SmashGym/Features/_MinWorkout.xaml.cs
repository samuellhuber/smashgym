﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Plugin.LocalNotifications;
using Plugin.Vibrate;
using Xamarin.Forms;

namespace SmashGym
{
    public partial class _MinWorkout : ContentPage
    {

        public _MinWorkout()
        {
            InitializeComponent();

            LblSet.IsVisible = false;
            LblEx.IsVisible = false;
            LblExName.IsVisible = false;
        }
        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to Main GymPage in TabbedBar (MainPage Tabbed Focus Home) 
            Application.Current.MainPage = new MainPage(0);
        }

        public void BttnStartClicked(object sender, EventArgs e)
        {
            if (EntryEx1.Text != "" && EntryEx2.Text != "" && EntryEx3.Text != "")
            {
                StartWorkout1();
            }
        }

        public void StartWorkout1()
        {
            //Set UI to workout
            EntryEx1.IsEnabled = false;
            EntryEx2.IsEnabled = false;
            EntryEx3.IsEnabled = false;
            BttnStart.IsEnabled = false;
            BttnBack.IsEnabled = false;

            LblSet.IsVisible = true;
            LblExName.IsVisible = true;
            LblEx.IsVisible = true;

            int i = 1; //Counter for currentExercise
            int j = 1; //Count rounds

            //Init Workout
            CrossLocalNotifications.Current.Show("START", "Es geht los!");
            LblExName.Text = EntryEx1.Text;
            LblSet.Text = "Satz " + j + " von 3";
            i = 2;

            // True = Repeat again, False = Stop the timer
            Device.StartTimer(TimeSpan.FromSeconds(30), () =>
            {

                LblSet.Text = "Satz " + j + " von 3";

                if (j == 3 && i == 4) //Break in last round means DONE
                {
                    CrossLocalNotifications.Current.Show("FERTIG", "Starkes Training!");
                    var v = CrossVibrate.Current;
                    v.Vibration(TimeSpan.FromSeconds(1));

                    //Set UI to non workout
                    LblSet.IsVisible = false;
                    LblExName.IsVisible = false;
                    LblEx.IsVisible = false;

                    BttnStart.IsEnabled = true;
                    BttnBack.IsEnabled = true;
                    EntryEx1.IsEnabled = true;
                    EntryEx2.IsEnabled = true;
                    EntryEx3.IsEnabled = true;

                    return false;
                }

                switch (i)
                {
                    case 1:
                        CrossLocalNotifications.Current.Show("PAUSE FERTIG", "MACH DICH RAN!");
                        LblExName.Text = EntryEx1.Text;
                        i++;
                        break;
                    case 2:
                        LblExName.Text = EntryEx2.Text;
                        i++;
                        break;
                    case 3:
                        LblExName.Text = EntryEx3.Text;
                        i++;
                        break;
                    case 4:
                        CrossLocalNotifications.Current.Show("PAUSE", "Gleich gehts weiter!");
                        LblExName.Text = "!PAUSE!";
                        i = 1;
                        j++;
                        var vibrate = CrossVibrate.Current;
                        vibrate.Vibration(TimeSpan.FromSeconds(1));
                        break;
                }//end of switch

                return true;
            }); //end of timer
        }

        //Notifications at start of exercise in 5MinWorkout
        private void ExNotification() 
        {
            CrossLocalNotifications.Current.Show("Fertig", "Nächste Übung!");
            var v = CrossVibrate.Current;
            v.Vibration(TimeSpan.FromSeconds(1));
        }
    }
}