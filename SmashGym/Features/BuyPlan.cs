﻿using System;
using System.Collections.Generic;

namespace SmashGym
{
    public class BuyPlan
    {
        //ProductID of the InAppPurchase
        public string productID { get; set; }
        //Downloadlink to xml
        public string url { get; set; }
        //Link to image
        public string image { get; set; }
        //Name of the Plan + File
        public string name { get; set; }
        //Description of Plan
        public string description { get; set; }
        //Price of Plan
        public string price { get; set; }
        //Already bought or not
        public bool bought { get; set; }

        public BuyPlan() { bought = false; image = "weight_std.png"; }

        public List<BuyPlan> getShopList()
        {
            //Initialize all Plans that can be bought
            List<BuyPlan> shopList = new List<BuyPlan>()
            {
                new BuyPlan(){ productID = "trainingsplan_00001", name = "Rücken, Sixpack", description = "Muskelaufbau, 3x pro Woche", price ="Free", url = "https://smashice.de/download/smashgym-ruecken-sixpack-3x-pro-woche/?wpdmdl=556&masterkey=5d382f9b532ac"},
                new BuyPlan(){ productID = "trainingsplan_00002", name = "Abnehmen & Sixpack", description = "Abnehmen, 2x pro Woche", price ="Free", url = "https://smashice.de/download/smashgym-abnehmen-sixpack-2x-pro-woche/?wpdmdl=552&masterkey=5d380b8b56227"},
                new BuyPlan(){ productID = "trainingsplan_00003", name = "Oberkörper Zerstören", description = "Muskelaufbau, 2x pro Woche", price ="Free", url = "https://smashice.de/download/smashgym-oberkoerper-zerstoeren-2x-pro-woche/?wpdmdl=553&masterkey=5d380be828c2a"},

            };

            return shopList;
        }
    }
}
