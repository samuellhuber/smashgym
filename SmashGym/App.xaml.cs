﻿using System;
using System.IO;
using Xamarin.Forms;
using Xamarin.Essentials;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SmashGym
{
    public partial class App : Application
    {
        public App()
        {
            ExperimentalFeatures.Enable(ExperimentalFeatures.EmailAttachments);//Enable sending EmailAttachments using Xamarin.Essentials

            InitializeComponent();

            //Init File for purchasable content
            if (!Directory.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/smashplan")))
                Directory.CreateDirectory(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/smashplan"));

            //If no user files exists create them
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "UserData.xml");
            if (!File.Exists(path))
            {
                User temp = new User();
                temp.Save();
            }
            else
            {
                User temp = User.DeserializeXMLFileToObject<User>(path);
                if (temp == null)
                {
                    temp = new User();
                    temp.Save();
                }
            }

            path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tempUserData.xml");
            if (!File.Exists(path))
            {
                User temp = new User();
                temp.SaveTemp();
            }

            MainPage = new MainPage();
        }
        
        protected override void OnStart()
        {
            //Handle when your app starts
            LoadWorkout();
        }

        protected override void OnSleep()
        {
            //Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        //Load stored variables for resuming workout - necessary due to backgrounding killing the app after 3min
        private async void LoadWorkout()
        {
            PlanSystem p = new PlanSystem();
            Plan temp = p.Load("tempplan.xml");

            if (temp != null)
            {
                bool resume = await Application.Current.MainPage.DisplayAlert("Moin", "Möchtest du dein Training fortsetzen?\nPlan: " + temp.pName, "Ja", "Nein");

                if (resume)
                    Application.Current.MainPage = new EnterWorkout(temp, temp.cData.cEx, temp.cData.cSet);
            }
        }
    }
}
