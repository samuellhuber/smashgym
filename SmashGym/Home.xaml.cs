﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Xamarin.Forms;
using Xamarin.Essentials;
using EmailMessage = Xamarin.Essentials.EmailMessage;
using EmailAttachment = Xamarin.Essentials.EmailAttachment;
using Plugin.Messaging;

namespace SmashGym
{
    public partial class Home : ContentPage
    {
        System.IO.DirectoryInfo ParentDirectory;
        System.IO.DirectoryInfo SmashPlans;
        PlanSystem p = new PlanSystem();
        User u = new User();

        public Home()
        {
            InitializeComponent();

            //Just open Homescreen one time for debugging reasons (Sharing violation)
            InitPickerCurrentPlan();

            //Load CurrentPlan and make it the selected in PickerCurrentPlan so that user doesnt always need to select the plan
            int cPlan = p.LoadCPlan();

            if (cPlan == -2) //If loaded from file set cPlan to Plan from file
                cPlan = PickerCurrentPlan.Items.Count - 1;

            PickerCurrentPlan.SelectedIndex = cPlan;

            if (cPlan >= PickerCurrentPlan.Items.Count)
            {
                PickerCurrentPlan.SelectedItem = -1;
            }

            MotivationQuotes m = new MotivationQuotes();
            LblMotivation.Text = m.Get();
        }

        public void InitPickerCurrentPlan()
        {
            ParentDirectory = new System.IO.DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData));
            foreach (System.IO.FileInfo f in ParentDirectory.GetFiles())
            {
                if (f.Name.Contains("xml") && f.Name != "UserData.xml" && f.Name != "tempUserData.xml")
                    PickerCurrentPlan.Items.Add(f.Name);
            }

            SmashPlans = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/smashplan/");
            foreach (System.IO.FileInfo f in SmashPlans.GetFiles())
            {
                PickerCurrentPlan.Items.Add(f.Name);
            }
        }

        public void BttnSharePlanClicked(object sender, EventArgs e)
        {
            //DisplayAlert("Coming Soon", "Coming Soon", "OK");
            if (PickerCurrentPlan.SelectedIndex != -1)
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex];
                if (File.Exists(path))
                {
                    EmailMessage message = new EmailMessage
                    {
                        Subject = "SmashGym Trainingsplan: " + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex],
                        Body = "Im Anhang findest du die Datei zu einem Trainingsplan für SmashGym (smashgym.smashice.de).\n" +
                        "Das SmashGym Team wünscht dir viel Spaß beim Trainieren!\n",
                    };

                    message.Attachments.Add(new EmailAttachment(path));

                    Email.ComposeAsync(message);
                }
                else
                    DisplayAlert("Fehler", "Gekaufte Pläne können nicht geteilt werden.", "Ok");
            }
            else
            {
                DisplayAlert("Fehler", "Bitte Plan auswählen", "OK");
            }

        }

        public async void BttnStartClicked(object sender, EventArgs e)
        {
            if (PickerCurrentPlan.SelectedIndex != -1)
            {
                Plan workoutPlan = new Plan();
                //If Plan exists go otherwise exit function
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]))
                    workoutPlan = p.Load((String)PickerCurrentPlan.SelectedItem);
                else if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/smashplan/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]))
                    workoutPlan = p.Load((String)"smashplan/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]);
                else
                {
                    await DisplayAlert("Fehler", "Plan konnte nicht gefunden werden!", "Ok");
                    return;
                }

                //Reset currentDay to 0 when all days have been completed once
                if (workoutPlan.cDay == workoutPlan.day.Length)
                {
                    workoutPlan.cDay = 0;
                }

                if (workoutPlan.cData.cancelled)
                {
                    bool resume = await DisplayAlert("Achtung!", "Möchtest du dein angefangenes Training fortsetzen?", "Ja", "Nein");

                    if (resume)
                    {
                        Application.Current.MainPage = new EnterWorkout(workoutPlan, workoutPlan.cData.cEx, workoutPlan.cData.cSet);
                    }
                    else
                    {
                        Application.Current.MainPage = new StartOverview(workoutPlan);
                    }
                }
                else { Application.Current.MainPage = new StartOverview(workoutPlan); }

                //Save selected Plan
                p.SaveCPlan(PickerCurrentPlan.SelectedIndex);
            }
            else { await DisplayAlert("Fehler", "Training kann nicht gestartet werden. Bitte Plan auswählen!", "OK"); }
        }

        public async void BttnDeleteClicked(object sender, EventArgs e)
        {
            if (PickerCurrentPlan.SelectedIndex != -1)
            {
                var answer = await DisplayAlert("Achtung!", "Wollen Sie den Plan wirklich dauerhaft löschen?", "Yes", "No");
                if (answer)
                {
                    if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]))
                        p.Delete(PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]);
                    else if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/smashplan/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]))
                        p.Delete("smashplan/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]);
                    else
                    {
                        await DisplayAlert("Fehler", "Plan konnte nicht gefunden werden!", "Ok");
                    }

                    PickerCurrentPlan.Items.Clear();
                    InitPickerCurrentPlan();
                }
            }
            else
            {
                await DisplayAlert("Fehler", "Plan auswählen!", "OK");
            }
        }

        public void BttnEditClicked(object sender, EventArgs e)
        {
            if (PickerCurrentPlan.SelectedIndex != -1)
            {
                if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/" + PickerCurrentPlan.Items[PickerCurrentPlan.SelectedIndex]))
                {
                    Plan Plan = p.Load((String)PickerCurrentPlan.SelectedItem);
                    Application.Current.MainPage = new CreatePlanUI(Plan);
                }
                else
                    DisplayAlert("Fehler", "Gekaufte Plänen können nicht bearbeitet werden.", "Ok");
            }
            else
                DisplayAlert("Fehler", "Du hast keinen Plan ausgewählt.", "Ok");
        }

        public void BttnCreatePlanClickedAsync(object sender, EventArgs e)
        {
            //Application.Current.MainPage = new CreatePlan();
            Application.Current.MainPage = new CreatePlanUI(); //new FLOW
        }
        public void Bttn1RMClicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new Calc1RM();
        }
        public void Bttn5minClicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new _MinWorkout();
        }

        void BttnHelpClicked(object sender, System.EventArgs e)
        {
            Uri url = new Uri("https://www.smashice.de");
            Device.OpenUri(url);
        }
    }
}
