﻿using System;
namespace SmashGym
{
    public class MotivationQuotes
    {

        private readonly string[] quotes = {
        "Get Them Weights - YP2",
        "In fitness, there are no short cuts. It involes immense discipline and hard work. - Mahesh Babu",
        "Fitness is not about being better than someone else.. It's about being better than you used to be. - Khloe Kardashian",
        "Physical fitness is not only one of the most important keys to a healthy body, it is the basis of dynamic and creative intellecutal activity. - JFK",
        "The human body is the best picture of the human soul. - Ludwig Wittgenstein",
        "The purpose of training is to tighten up the slack, toughen the body and polish the spirit. - Morihei Ueshiba",
        "Take care of your body. It's the only place you have to live. - Jim Rohn",
        "Strength comes from an indomitable will. - Ghandi",
        "Training gives us an outlet for suppressed energies created by stress and thus tones the spirit just as exercise conditions the body. - Arnold Schwarzenegger",
        "Your body can stand almost anything. It's your mind that you have to convince. - Unkown",
        "Fitness is like a relationship. You can’t cheat and expect it to work. - Unkown",
        "Respect your body. It’s the only one you get. - Unkown",
        "The worst I can be is the same as everybody else. - Arnold Schwarzenegger",
        "Dead last finish is greater than did not finish, which trumps did not start. - Unkown",
        "The best way to predict the future is to create it. - Abraham Lincoln",
        "Work hard in silence. Let success be your noise. - Frank Ocean",
        "Rome wasn’t built in a day, but they worked on it every single day. - Unkown",
        "The difference between try and triumph is a little umph. – Marvin Phillips",
        "To be succesful, you must dedicate yourself 100% to your training, diet and mental approach. - Arnold Schwarzenegger",
        "In my judgment, physical fitness is basic to all forms of excellence. - Robert Kennedy",
        "Nobody is perfect, but life is about choices. - LL Cool J",
        "Strengt does not come from winning. Your struggles develop your strengths. - Arnold Schwarzenegger",
        "Everybody wants to be a bodybuilder but nobody wants to lift heavy ass weights! - Ronnie Coleman",
        "If you’re capable of sending a legible text message between sets, you probably aren’t working hard enough. - Dave Tate",
        "The road to nowhere is paved with excuses. - Mark Bell",
        "I hated every minute of training, but I said, don’t quit. Suffer now and live the rest of your life as a champion. - Mahummad Ali",
        "Pain is temporary, pride is forever. - Unkown",
        "Biceps are like ornaments on a Christmas tree. - Ed Coan",
        "If you want something you’ve never had, you must be willing to do something you’ve never done. - Thomas Jefferson",
        "I'm not sold on one diet philosophy. I'm sold on whatever will work for you. - Dave Tate",
        "If you think lifting weights is dangerous, try being weak. Being weak is dangerous. - Bret Contreras",
        "Today I will do what others won’t, so tomorrow I can accomplish what others can’t. —Jerry Rice",
        };

        public MotivationQuotes()
        {
        }

        public string Get()
        {
            string quote = "";

            DateTime _now = DateTime.Now;
            int day = _now.Day;
            quote = quotes[day];

            return quote;
        }
    }
}
