﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace SmashGym
{
    public partial class CreatePlan : ContentPage
    {
        Plan createPlan; //Plan to be created

        public CreatePlan()
        {
            InitializeComponent();

            //Disable controls for safe flow
            PickerDay.IsEnabled = false;
            EntryNameOfDay.IsEnabled = false;
            PickerExercises.IsEnabled = false;
            BttnConfirmDay.IsEnabled = false;
            BttnConfirmInit.IsEnabled = false;

            //Add Days to PickerDays
            for (int i = 1; i <= 7; i++)
            {
                PickerDays.Items.Add(i.ToString());
            }

            //Add Exercises to PickerExercises
            for (int i = 1; i <= 10; i++)
            {
                PickerExercises.Items.Add(i.ToString());
            }
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to Main GymPage in TabbedBar (MainPage Tabbed Focus Gym) 
            Application.Current.MainPage = new MainPage(1);
        }

        public void BttnConfirmClicked(object sender, EventArgs e)
        {
            //Crate new Class Plan() and save data
            if (EntryPlanName.Text != "" && PickerDays.SelectedIndex != -1)
            {
                UInt16 nDays = Convert.ToUInt16(PickerDays.Items[PickerDays.SelectedIndex]); //amount of Days
                Console.WriteLine("Planname: |" + EntryPlanName.Text + "   Days: |" + PickerDays.Items[PickerDays.SelectedIndex]);
                createPlan = new Plan(EntryPlanName.Text, nDays);
                for (int i = 1; i <= nDays; i++)
                {
                    PickerDay.Items.Add(i.ToString());
                }
                //enable next step in flow
                PickerDay.IsEnabled = true;
                EntryNameOfDay.IsEnabled = true;
                PickerExercises.IsEnabled = true;
                BttnConfirmDay.IsEnabled = true;
                //disable old step in flow
                EntryPlanName.IsEnabled = false;
                PickerDays.IsEnabled = false;
                BttnConfirm.IsEnabled = false;
            }
            else { DisplayAlert("Fehler", "Überprüfe deine Eingabe!", "OK"); }
        }

        public void PickerDayChanged(object sender, EventArgs e)
        {
            int day = Convert.ToUInt16(PickerDay.Items[PickerDay.SelectedIndex]) - 1;
            //Set other UI elements to match Days[day]
            if (createPlan.day[day] != null)
            {
                EntryNameOfDay.Text = createPlan.day[day].dName;
                if (createPlan.day[day].Exercise != null)
                {
                    PickerExercises.SelectedIndex = createPlan.day[day].Exercise.Length;
                }
                else
                {
                    PickerExercises.SelectedIndex = -1;
                }
            }
        }

        public void BttnConfirmDayClicked(object sender, EventArgs e)
        {
            if (PickerDay.SelectedIndex != -1 && EntryNameOfDay.Text != "" && PickerExercises.SelectedIndex != -1)
            {
                //Init selected Day of the currentPlan
                int day = Convert.ToInt32(PickerDay.Items[PickerDay.SelectedIndex]);
                day = day - 1; //to correct for arrays starting with 0 and not with 1 like the Picker
                //Init Day
                createPlan.day[day].dName = EntryNameOfDay.Text;
                createPlan.day[day].Exercise = new Exercises[Convert.ToUInt16(PickerExercises.Items[PickerExercises.SelectedIndex])];

                //Init Exercises in selected day
                for (int i = 0; i < createPlan.day[day].Exercise.Length; i++)
                {
                    createPlan.day[day].Exercise[i] = new Exercises();
                }
                //Enable next flow
                BttnConfirmInit.IsEnabled = true;
                //NO need to disable old flow as this can be run multiple times and next flow automatically loads new page
            }
            else
            {
                DisplayAlert("Fehler", "Bitte Felder ausfüllen.", "OK");
            }
        }

        public void BttnConfirmInitClicked(object sender, EventArgs e)
        {
            bool test = false;

            //Info supersets
            DisplayAlert("INFO", "Satzpause 0, erstellt Supersatz mit nächster Übung", "OK");

            foreach (Days d in createPlan.day)
            {
                Console.WriteLine(d.dName);
                if (d.dName != null)
                {
                    test = true;
                }
                else
                {
                    test = false;
                    break;
                }
            }
            //IF Days and Plan correctly created
            if (test)
            {
                Application.Current.MainPage = new CreateDays(createPlan);
            }
            else
            {
                DisplayAlert("Fehler", "Tage nicht korrekt erstellt! Überprüfe, ob du für alle Tage eingetragen hast.", "OK");
            }
        }
    }
}
