﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Xml.Serialization;

using Xamarin.Forms;

namespace SmashGym
{
    public partial class CreateDays : ContentPage
    {
        private Plan createPlan; //Plan to be created
        UInt16 day = 1;
        UInt16 ex = 0;

        public CreateDays()
        {
            InitializeComponent();
        }

        public CreateDays(Plan _createPlan)
        {
            InitializeComponent();
            createPlan = _createPlan;

            //Set PickerDay
            for (int i = 1; i <= createPlan.day.Length; i++)
            {
                PickerDay.Items.Add(i.ToString());
            }
        }

        public void BttnBackClicked(object sender, EventArgs e)
        {
            //Go Back to Main GymPage in TabbedBar (MainPage Tabbed Focus Gym) 
            Application.Current.MainPage = new MainPage(1);
        }

        public void PickerDayChanged(object sender, EventArgs e)
        {
            day = Convert.ToUInt16(PickerDay.Items[PickerDay.SelectedIndex]);
            day = --day;

            //Set other UI elements to match Days[day]
            if (PickerExercise.SelectedIndex != -1 && PickerDay.SelectedIndex != -1)
            {
                ex = 0;
                EntryNameOfExcercise.Text = createPlan.day[day].Exercise[ex].exName;
                EntrySets.Text = createPlan.day[day].Exercise[ex].sets.ToString();
                EntryReps.Text = createPlan.day[day].Exercise[ex].reps.ToString();
                EntryPause.Text = createPlan.day[day].Exercise[ex].pause.ToString();
            }
            if (PickerDay.SelectedIndex != -1)
            {
                PickerExercise.Items.Clear();

                //Set PickerExercise
                for (int i = 1; i <= createPlan.day[day].Exercise.Length; i++)
                {
                    PickerExercise.Items.Add(i.ToString());
                }
                PickerExercise.SelectedIndex = 0;
            }

        }

        public void PickerExerciseChanged(object sender, EventArgs e)
        {
            //Set UI elements to current exercise
            if (PickerExercise.SelectedIndex != -1 && PickerDay.SelectedIndex != -1)
            {
                //Set ex
                ex = Convert.ToUInt16(PickerExercise.Items[PickerExercise.SelectedIndex]);
                ex = --ex;


                EntryNameOfExcercise.Text = createPlan.day[day].Exercise[ex].exName;
                EntrySets.Text = createPlan.day[day].Exercise[ex].sets.ToString();
                EntryReps.Text = createPlan.day[day].Exercise[ex].reps.ToString();
            }
        }

        //Make sure that Sets can only be Numbers
        public void EntrySetsTextChanged(object sender, EventArgs e)
        {
            if (EntrySets.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntrySets.Text = ""; }
        }

        //Make sure that Reps can only be Numbers
        public void EntryRepsTextChanged(object sender, EventArgs e)
        {
            if (EntryReps.Text.All(char.IsDigit)) { }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryReps.Text = ""; }
        }

        //Make sure Pause can only be Numbers
        public void EntryPauseTextChanged(object sender, EventArgs e)
        {
            if (EntryPause.Text.All(char.IsDigit))
            {
                if (EntryPause.Text != "")
                {
                    if (Int32.Parse(EntryPause.Text) > 180)
                    {
                        DisplayAlert("Fehler", "Pause darf maximal 3 Minuten sein. Grund: IOS schließt sonst die App, wenn du zu lange rausgehst.", "OK"); EntryPause.Text = "";
                    }
                }
            }
            else { DisplayAlert("Fehler", "Bitte nur Zahlen eingeben!", "OK"); EntryPause.Text = ""; }
        }

        public void BttnConfirmExClicked(object sender, EventArgs e)
        {
            if (EntryNameOfExcercise.Text != "" && Convert.ToInt16(EntrySets.Text) > 0 && Convert.ToInt16(EntryReps.Text) > 0)
            {
                createPlan.day[day].Exercise[ex].exName = EntryNameOfExcercise.Text;
                createPlan.day[day].Exercise[ex].sets = Convert.ToUInt16(EntrySets.Text);
                createPlan.day[day].Exercise[ex].reps = Convert.ToUInt16(EntryReps.Text);
                createPlan.day[day].Exercise[ex].weightDone = new float[Convert.ToUInt16(EntrySets.Text)];
                createPlan.day[day].Exercise[ex].repsDone = new uint[Convert.ToUInt16(EntrySets.Text)];
                createPlan.day[day].Exercise[ex].pause = Convert.ToUInt16(EntryPause.Text);
                //Enables supersets
                if (createPlan.day[day].Exercise[ex].pause == 0)
                {
                    createPlan.day[day].Exercise[ex].superset = true;
                }
                //automatically go through exercises & days when confirmed
                if (PickerExercise.SelectedIndex < PickerExercise.Items.Count())
                {
                    PickerExercise.SelectedIndex++;

                    if (PickerDay.SelectedIndex < PickerDay.Items.Count() && PickerExercise.SelectedIndex == PickerExercise.Items.Count() - 1)
                    {
                        PickerDay.SelectedIndex++;
                        PickerExercise.SelectedIndex = 0;
                    }
                }
            }
            else { DisplayAlert("Fehler", "Bitte alle Felder füllen!", "OK"); }
        }

        public void BttnConfirmAllClicked(object sender, EventArgs e)
        {
            bool test = false;
            foreach (Days d in createPlan.day)
            {
                foreach (Exercises exercise in d.Exercise)
                {
                    if (exercise.reps != 0 && exercise.exName != "")
                    {
                        test = true;
                    }
                    else
                    {
                        test = false;
                        break;
                    }
                }
            }
            if (test)
            {
                //Save the Plan to an XML aka create Plan
                PlanSystem p = new PlanSystem();
                p.Create(createPlan);

                //Go to Mainpage (Gym)
                Application.Current.MainPage = new MainPage(1);
            }
            else { DisplayAlert("Fehler", "Bitte für alle Tage, alle Übungen eintragen!", "OK"); }
        }
    }
}
