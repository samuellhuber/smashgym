﻿using System;
using System.Net;
using System.Collections.Generic;

using Plugin.DownloadManager;
using Xamarin.Forms;
using System.IO;
using Plugin.DownloadManager.Abstractions;
using System.Threading.Tasks;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using System.Diagnostics;

namespace SmashGym
{
    public partial class Gym : ContentPage
    {
        public Gym()
        {
            InitializeComponent();

            LVmain.ItemsSource = new BuyPlan().getShopList();
        }

        async void LVmain_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            //PopUp to Buy Item
            var selectedItem = e.Item as BuyPlan;

            //Init Download
            string path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "/smashplan/" + selectedItem.name + ".xml");
            string url = selectedItem.url;
            
            //Purchase InAppPurchase
            bool temp = await MakePurchase(selectedItem.productID, url, path);

            if (temp == true)
                await DisplayAlert("DANKE", "Vielen Dank für die Unterstützung. Nur so ist es möglich die App zu erhalten. Dein Plan steht jetzt zum Training bereit. (Home Seite)", "Gerne");
            else
                await DisplayAlert("Schade", "Der Kauf konnte nicht abgewickelt werden.", "Ok");

            /* ------- FOR DEBUGGING WITH SIMULATOR ONLY --------
            //Download File
            var downloadManager = CrossDownloadManager.Current;
            downloadManager.PathNameForDownloadedFile = new Func<IDownloadFile, string>(filePath => { return path; });
            var file = downloadManager.CreateDownloadFile(url);
            downloadManager.Start(file, true);*/

            var lv = sender as ListView;
            lv.SelectedItem = null;
        }

        public async Task<bool> MakePurchase(string _productID, string url, string path)
        {
            string payload = "SmashIceBestIce";
            string productID = _productID;
            var billing = CrossInAppBilling.Current;

            try
            {
                //Check if Plugin works on current Platform
                if (!CrossInAppBilling.IsSupported)
                    return false;

                //Check for connection
                var connected = await billing.ConnectAsync(ItemType.InAppPurchase);
                if (!connected)
                    return false;

                var verify = DependencyService.Get<IInAppBillingVerifyPurchase>();
                var purchase = await billing.PurchaseAsync(productID, ItemType.InAppPurchase, payload, verify);
                //possibility that a null came through.
                if (purchase == null)
                {
                    //did not purchase
                    return false;
                }
                else if (purchase.State == PurchaseState.Purchased)
                {
                    //purchased!
                    //Download File
                    var downloadManager = CrossDownloadManager.Current;
                    downloadManager.PathNameForDownloadedFile = new Func<IDownloadFile, string>(filePath => { return path; });
                    var file = downloadManager.CreateDownloadFile(url);
                    downloadManager.Start(file, true);

                    Home h = new Home();
                    h.InitPickerCurrentPlan();

                    return true;
                }
            }
            catch (InAppBillingPurchaseException purchaseEx)
            {
                //Billing Exception handle this based on the type
                Debug.WriteLine("Error: " + purchaseEx);
            }
            catch (Exception ex)
            {
                //Something else has gone wrong, log it
                Debug.WriteLine("Issue connecting: " + ex);
            }
            finally
            {
                await billing.DisconnectAsync();
            }
            return false;
        }

    }
}
