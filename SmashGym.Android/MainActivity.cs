﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Plugin.InAppBilling;

namespace SmashGym.Droid
{
    [Activity(Label = "SmashGym", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, LaunchMode = Android.Content.PM.LaunchMode.SingleTask,
    ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]

    [IntentFilter(new[] { Intent.ActionView},
        Categories = new[] { Intent.CategoryDefault },
        DataScheme = "file",
        DataHost = "*",
        DataPathPattern = ".*\\.xml",
        DataMimeType = @"application/xaml+xml")]

    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            string action = Intent.Action;
            string type = Intent.Type;

            //If opened to recieve a file
            if (Intent.ActionView.Equals(action) && !String.IsNullOrEmpty(type))
            {
                Console.WriteLine("Opened to recieve a file!");
            }

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            OxyPlot.Xamarin.Forms.Platform.Android.PlotViewRenderer.Init();
            LoadApplication(new App());
        }
        //Make InAppBilling Plugin able to work properly
        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            InAppBillingImplementation.HandleActivityResult(requestCode, resultCode, data);
        }
    }
}